#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED

struct Contact{
    int id;
    char first_name[256];
    char last_name[256];
    char address[255];
    char email[255];
    char phone[15];
    char mobile[15];
};

#endif // STRUCTURES_H_INCLUDED
