#include <stdio.h>
#include <string.h>
#include "operations.h"
#include "structures.h"


struct Contact address_table[MAX_RECORD];
int counter = 0;

void displayProgramMenu(){
    /*
     * Display the program menu
     */
    printf("***********************************************\n");
    printf("*        AddressBook Program Menu             *\n");
    printf("***********************************************\n");
    printf("*                                             *\n");
    printf("*   1. Add        2. Edit       3. Delete     *\n");
    printf("*   4. View     5. View All   6. Save       *\n");
    printf("*                 0. Exit                     *\n");
    printf("*                                             *\n");
    printf("***********************************************\n");
    printf("Select an action: ");


}
void displayContact(struct Contact contact){
    printf("Full Name: %s %s\n",contact.first_name,contact.last_name);
    printf("Address: %s\n",contact.address);
    printf("Email: %s\n",contact.email);
    printf("Phone: %s\n",contact.phone);
    printf("Mobile: %s\n",contact.mobile);
}

void executeAction(int action){
    /*
     * Executes different functions based on selected action menu
     */
    switch(action){
        case 0:
            printf("Program closed.\n");

            break;
        case 1:
            //Add contact record
            if(counter<MAX_RECORD){
                addContact();
            }else{
                printf("No more space to add contact.\n");
            }

            break;
        case 2:{ //Edit a contact record
                if(counter==0){
                    printf("No records found.\n");
                }else{
                    int position_to_edit=0;
                    printf("Enter the position of the record to be edit: ");
                    scanf("%d",&position_to_edit);
                    if(position_to_edit<=counter && position_to_edit>0){
                        editContact(position_to_edit-1);
                        printf("The record at %d was updated.\n",position_to_edit);
                    }else{
                        printf("Contact doesn't exist.\n");
                    }
                }
           }
           break;
        case 3:{ //Delete a contact record
                if(counter==0){
                    printf("No records found.\n");
                }else{
                    int position_to_delete=0;
                    printf("Enter the position of the record to be deleted: ");
                    scanf("%d",&position_to_delete);
                    if(position_to_delete<=counter && position_to_delete>0){
                        printf("The record to delete is at position: %d.\n",position_to_delete);
                        int tempPosition = position_to_delete - 1;
                        deleteContact(tempPosition);
                        printf("The record at %d was deleted.\n",position_to_delete);
                    }else{
                            printf("Contact doesn't exist.\n");
                    }
                }
            }
            break;
        case 4:{
                if(counter==0){
                    printf("No records found.\n");
                }else{
                    int position_to_view=0;
                    printf("Enter the position of the record to view: ");
                    scanf("%d",&position_to_view);
                    if(position_to_view<=counter && position_to_view>0){
                        displayContact(address_table[position_to_view-1]);
                    }else{
                        printf("Contact doesn't exist.\n");
                    }
                }

            }
            break;
        case 5:{
                //Display all the contact records
                if(counter==0){
                    printf("No records found.\n");
                }else{
                    int i;
                    for(i=0;i<counter;i++){
                        printf("Position: %d\n",i+1);
                        displayContact(address_table[i]);
                        printf("---------------------------\n");
                    }
                }
            }
            break;
        case 6:
            if(counter==0){
                printf("No records to save.\n");
            }else{
                saveDatabase();
            }
            break;

        default:
            printf("Please enter valid action.");
            break;
    }

}
void saveDatabase(){
    FILE *fp;
    int i;
    fp = fopen("addresses.csv", "w+");
    if(fp){
            fputs("First Name,Last Name,Email,Address,Phone,Mobile\n",fp);
        for(i=0;i<counter;i++){
            struct Contact contact=address_table[i];
            char *result = malloc(strlen(contact.first_name)+strlen(contact.last_name)+strlen(contact.address)+strlen(contact.email)+strlen(contact.phone)+strlen(contact.mobile)+6);
            strcpy(result, contact.first_name);
            strcat(result,",");
            strcat(result, contact.last_name);
            strcat(result,",");
            strcat(result, contact.email);
            strcat(result,",");
            strcat(result, contact.address);
            strcat(result,",");
            strcat(result, contact.phone);
            strcat(result,",");
            strcat(result, contact.mobile);
            strcat(result,"\n");

            fputs(result,fp);
            free(result);
        }
        fclose(fp);
        printf("Saved to addresses.txt\n");
    }else{
        printf("Failed to save.\n");
    }

}



void exportContact(){
    printf("Export a contact\n");
}

void searchContact(){
    printf("Search a contact\n");
}

void addContact(){

    struct Contact contact;
    printf("Enter first name: ");
    scanf(" %[^\t\n]s",contact.first_name);
    printf("Enter last name: ");
    scanf("%s",&contact.last_name);
    printf("Enter email: ");
    scanf("%s",&contact.email);
    printf("Enter address: ");
    scanf(" %[^\t\n]s",contact.address);
    //scanf("%s",&contact.address);
    printf("Enter phone: ");
    scanf("%s",&contact.phone);
    printf("Enter mobile: ");
    scanf("%s",&contact.mobile);
    //displayContact(contact); //Uncomment to display the contact info that will be saved
    printf("contact added to position %d\n",counter+1);
    address_table[counter] = contact; //save the value to array
    counter++; //increment the counter

}

void editContact(int position){
     int done=0,edit;
     struct Contact contact = address_table[position];
     while(done!=1){ //Allow edit options until done
        char ans;
        printf("-----------------------------------------------\n");
        printf("         What do you want to edit?             \n");
        printf("-----------------------------------------------\n");
        printf("  1. First Name    2. Last Name    3. Email    \n");
        printf("  4. Address       5. Phone        6. Mobile   \n");
        printf("                 0. Finish editing             \n");
        printf("-----------------------------------------------\n");
        printf("Select an action: ");
        scanf("%d",&edit);
        switch(edit){
        case 1:
            printf("Current first name: %s\n",contact.first_name);
            printf("Enter new first name: ");
            scanf("%s",&contact.first_name);
            break;
        case 2:
            printf("Current last name: %s\n",contact.last_name);
            printf("Enter new last name: ");
            scanf("%s",&contact.last_name);
            break;
        case 3:
            printf("Current email: %s\n",contact.email);
            printf("Enter new email: ");
            scanf("%s",&contact.email);
            break;
        case 4:
            printf("Current address: %s\n",contact.address);
            printf("Enter new address: ");
            scanf("%s",&contact.address);
            break;
        case 5:
            printf("Current phone: %s\n",contact.phone);
            printf("Enter new phone: ");
            scanf("%s",&contact.phone);
            break;
        case 6:
            printf("Current mobile: %s\n",contact.mobile);
            printf("Enter new mobile: ");
            scanf("%s",&contact.mobile);
            break;
        default:
            done=1;
        }

        address_table[position]=contact;
     }
}

void deleteContact(int position){
        //Contact temp=address_table[position]; //can be saved to a temporary vairable if required to do any kind of processing
        int i;
        if(position>=0 && position<=counter){ //records need to be moved only if the position is not 0 and greater than counter
             for(i=position;i<counter;i++){ //loop to move all record after the deleted to position to one step forward
                //printf("Move record at %d from %d",i,i+1);
                address_table[i]=address_table[i+1]; //
            }
        }
        counter--;

}
