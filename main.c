#include <stdio.h>
#include <stdlib.h>
#include "operations.h"
#include "structures.h"


int exit_app=0;
int main()
{
    int action=0;
    while(!exit_app){
        displayProgramMenu();
        scanf("%d",&action);
        if(action==0){
            exit_app = 1;
        }
        executeAction(action);
    }
    return 0;

}
