#ifndef OPERATIONS_H_INCLUDED
#define OPERATIONS_H_INCLUDED
#include "structures.h"

#define MAX_RECORD 10

void displayProgramMenu();
void displayContact(struct Contact contact);
void executeAction(int);
void saveDatabase();
void exportContact();
void searchContact();
void addContact();
void editContact(int position);
void deleteContact(int position);


#endif // OPERATIONS_H_INCLUDED
